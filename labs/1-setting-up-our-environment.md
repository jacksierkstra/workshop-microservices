# Setting up our environment

## Starting point
```
↳ git checkout 1-setting-up-our-environment
```

We're going to start to create three different apps which we will going to run through `Docker`.

Start by creating a `code/` folder under the project root.

We're going to create three services with the names: `service-billing`, `service-document-generation` and `service-email`. It should look like below.

Make three apps with the [Spring Initializr](https://start.spring.io/) and only add Spring Web as a dependency. Use the service names as listed above. Below you can see an example of it.

![Spring initializr settings](img/spring-initializr-settings.png "Spring initializr settings")

If you download the package, you will end up with a `.zip`-file for each service.
Unzip it and then copy the contents to the `code/`-folder you made earlier.

![End result](img/end-result.png "End result")

To verify you did everything correctly, you can open up the service by opening the `pom.xml`-file with IntelliJ IDEA (Open as Project) and see if the app starts up.

Below you can see I'm starting `service-billing`, but the
other services should be starting by as well with the same steps.

![Finishing point](img/finishing-point.png "Finishing point")

## Assignment
Follow above steps so you have all the correct apps in their corresponding folder. Also add a `HelloAppController` to each of the apps so we can distinguish them later.

Make each app return a Json object with the following format (this is an example):
```json
{
    "app" : "{service-name}",
    "message" : "Hello"
}
```
- service-billing should have a GET endpoint that returns a Json object with the above specification.
- service-document-generation should have a GET endpoint that returns a Json object with the above specification.
- service-email should have a GET endpoint that returns a Json object with the above specification.

## Finishing point
If you want to skip ahead, get the latest code from this assignment.
```
↳ git checkout 1-setting-up-our-environment-finished
```

## Next lab
You are now ready to proceed to the next lab: 
* [Dockerizing our apps](2-dockerizing-our-apps.md)
