# Connecting it up

## Previous lab
 * [Running our apps](3-running-our-apps.md)

## Starting point
```
↳ git checkout 4-connecting-it-up
```

In the previous labs we saw that we can build Docker containers for our apps and we can run them. This is great to check whether it runs, but it gets very tedious very quick. So we are going to automate this some more with `Docker-Compose`.

We are going to start by creating a folder called `deployment/` in the root of this repository. In that folder we are going to create a file called `docker-compose.yml`.

In this file, we can define our service definitions. A very simple example of this would be the following.
```yaml
version: '3.3'
services:
  service-billing: # <--- this is the name of the service and should be unique.
    image: service-billing:latest # <--- this is the image of which the service will be based upon.
    hostname: service-billing # <--- this will be the hostname it will be known as.
    restart: always # <--- if it fails, always restart.
    ports:
      - 8081:8080 # <-- bind the host port 8081 to whatever is running in the container on 8080.
``` 

Now whenever you tell `Docker-Compose` to run this file, it starts the containers according to this definition. Now run the below command after adding this file from the root of this repository.

```
↳ docker-compose -f deployment/docker-compose.yml up -d
```

This will start up the container we created earlier.
```
↳ docker ps
CONTAINER ID        IMAGE                    COMMAND                  CREATED             STATUS              PORTS                    NAMES
80e51c020246        service-billing:latest   "java -cp /app/resou…"   9 minutes ago       Up 9 minutes        0.0.0.0:8081->8080/tcp   deployment_service-billing_1
```

Again, we can do a GET request to this container just like before.

```
↳ http :8081/hello
HTTP/1.1 200 
Connection: keep-alive
Content-Type: application/json
Date: Thu, 19 Mar 2020 18:35:17 GMT
Keep-Alive: timeout=60
Transfer-Encoding: chunked

{
    "app": "service-billing",
    "message": "Hello"
}
```

And if you want to take everything down.
```
↳ docker-compose -f deployment/docker-compose.yml down                      
Stopping deployment_service-billing_1 ... done
Removing deployment_service-billing_1 ... done
Removing network deployment_default
```

## Assignment
We have made `service-billing` part of our `Docker-Compose` definition but we still need our other services. Can you make sure that they are added to the definition? And that they all run and are able to reach?
 
## Finishing point
If you want to skip ahead, get the latest code from this assignment.
```
↳ git checkout 4-connecting-it-up-finished
```

## Next lab
 * [Intercommunication](5-intercommunication.md)