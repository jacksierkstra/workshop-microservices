# Intercommunication

## Previous lab
 * [Connecting it up](4-connecting-it-up.md)

## Starting point

```
↳ git checkout 5-intercommunication
```

Now that we have our services running, we would like them to communicate with each other. We can do this in a couple of ways:

- HTTP Requests
- Messaging
- [gRPC](https://grpc.io/)

Whereas HTTP requests is the most simple and straightforward, we're going to use messaging because it is loosely coupling our services and that makes it very flexible. So if a service is interested in a message, it just makes itself available as a listener.

gRPC is actually somewhat more advanced because you can define a contract between a client and server and generate code from that. For the purpose of demonstration it is not a good option because it is hard(er) to setup.

In order to make use of messaging, we need a message broker. We're going to use RabbitMQ because it is a simple and easy to setup broker and it has a prebuilt [Docker container](https://hub.docker.com/_/rabbitmq) for us. But before we are starting with that, we need to prepare our services to be able to send and receive messages.

So what do we want to achieve here? We would like to start sending an event and we just subscribe to it with other services. This makes it very flexible and from a service's perspective, you subscribe to whatever you are interested in and receive that whenever you're ready.

This is the plan:

- Do a GET request to `service-billing`.
- `service-billing` creates a message which it will publish to the message broker.
- `service-email` will receive the message and does whatever logic it needs to do.
- `service-document-generation` will receive the message and does whatever logic it needs to do.

Also, when a service is down, it does not matter because the message broker keeps the message until it is delivered.

### Adding the messagebroker
Pick the `docker-compose.yml` from the previous lab assignment and add the following to it:
```yaml
version: '3.3'

services:
  message-broker:
    image: rabbitmq:3.8-management
    hostname: message-broker
    restart: always
    ports:
      - "5672:5672"
      - "15672:15672"
```
This will add a RabbitMQ container to our setup. It also has a nice management console which will be available on port `15672`. If you start this and go to your browser, you should be able to go to `http://localhost:15672/#/` and you can login with username `guest` and password `guest`.

### Setting up the sender
Start by adding this dependency to `service-billing`'s `pom.xml`.
```xml
<dependency>
    <groupId>org.springframework.boot</groupId>
    <artifactId>spring-boot-starter-amqp</artifactId>
</dependency>
```

> TIP: If you are using IntelliJ you can enable [auto-import](https://www.jetbrains.com/help/idea/maven-importing.html) for Maven. That way Maven will get fetch the dependency for you automatically. Otherwise you have to build the application everytime you change something to your `pom.xml`.

Now we need some configuration in order to start sending the messages. Start by putting below configuration in your `application.properties`.
```
spring.rabbitmq.host=message-broker
spring.rabbitmq.port=5672
spring.rabbitmq.username=guest
spring.rabbitmq.password=guest

spring.main.allow-bean-definition-overriding=true

workshop.rabbitmq.queue=workshop.events.billing
workshop.rabbitmq.exchange=workshop.exchange
workshop.rabbitmq.routingkey=workshop.routingkey
```

Next step is to add configuration to the application to be able to send to the messagebroker. Add the following class to your app:

```java
import org.springframework.amqp.rabbit.connection.ConnectionFactory;
import org.springframework.amqp.core.*;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.amqp.support.converter.Jackson2JsonMessageConverter;
import org.springframework.amqp.support.converter.MessageConverter;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class MessageBrokerConfiguration {

    @Value("${workshop.rabbitmq.queue}")
    private String queueName;
    @Value("${workshop.rabbitmq.exchange}")
    private String exchange;
    @Value("${workshop.rabbitmq.routingkey}")
    private String routingKey;

    @Bean
    public Queue queue() {
        return new Queue(queueName);
    }

    @Bean
    public DirectExchange exchange() {
        return new DirectExchange(exchange);
    }

    @Bean
    Binding binding(Queue queue, DirectExchange exchange) {
        return BindingBuilder.bind(queue).to(exchange).with(routingKey);
    }

    @Bean
    public MessageConverter jsonMessageConverter() {
        return new Jackson2JsonMessageConverter();
    }

    @Bean
    public AmqpTemplate rabbitTemplate(ConnectionFactory connectionFactory) {
        final RabbitTemplate rabbitTemplate = new RabbitTemplate(connectionFactory);
        rabbitTemplate.setMessageConverter(jsonMessageConverter());
        return rabbitTemplate;
    }
}
```

We want something to send to our consumers so let's make that. Create a package `domain` and add below class.

```java
package com.workshop.servicebilling.domain;

import java.math.BigDecimal;

public class StartedBillingEvent {
    private String customerName;
    private BigDecimal amountInEuros;

    public String getCustomerName() {
        return customerName;
    }

    public void setCustomerName(String customerName) {
        this.customerName = customerName;
    }

    public BigDecimal getAmountInEuros() {
        return amountInEuros;
    }

    public void setAmountInEuros(BigDecimal amountInEuros) {
        this.amountInEuros = amountInEuros;
    }
}
```

Now we are going to create a mechanism to actually send the messages. So, the `HelloAppController` we made earlier should look below. And whenever we do a GET request to `/hello` it should send a message to the message broker.

```java
package com.workshop.servicebilling.controller;

import com.workshop.servicebilling.domain.Greeting;
import com.workshop.servicebilling.domain.StartedBillingEvent;
import org.springframework.amqp.core.AmqpTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.math.BigDecimal;

@RestController
public class HelloAppController {

    @Autowired
    private AmqpTemplate rabbitTemplate;

    @Value("${workshop.rabbitmq.exchange}")
    private String exchange;
    @Value("${workshop.rabbitmq.routingkey}")
    private String routingkey;

    @RequestMapping(value = "/hello", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public Greeting hello() {
        Greeting greeting = new Greeting();
        greeting.setMessage("Hello");
        greeting.setApp("service-billing");
        return greeting;
    }

    @RequestMapping(value = "/start-billing", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity startBilling() {
        StartedBillingEvent startedBillingEvent = new StartedBillingEvent();
        startedBillingEvent.setCustomerName("John Doe");
        startedBillingEvent.setAmountInEuros(BigDecimal.valueOf(50));
        rabbitTemplate.convertAndSend(exchange, routingkey, startedBillingEvent);
        return ResponseEntity.ok().build();
    }
}
```

### Setting up the listener(s)
Now we're going to setup both `service-document-generation` and `service-email` to listen to the messages sent by `service-billing`. So apply this to both! Also, make sure you added the dependency to the `pom.xml` of each service:

```xml
<dependency>
    <groupId>org.springframework.boot</groupId>
    <artifactId>spring-boot-starter-amqp</artifactId>
</dependency>
```

Add this to `service-document-generations`'s `application.properties`.
```
spring.rabbitmq.host=message-broker
spring.rabbitmq.port=5672
spring.rabbitmq.username=guest
spring.rabbitmq.password=guest

spring.main.allow-bean-definition-overriding=true

workshop.rabbitmq.queue=workshop.events.document-generation
workshop.rabbitmq.exchange=workshop.exchange
workshop.rabbitmq.routingkey=workshop.routingkey
```

Add this to `service-email`'s `application.properties`.
```
spring.rabbitmq.host=message-broker
spring.rabbitmq.port=5672
spring.rabbitmq.username=guest
spring.rabbitmq.password=guest

spring.main.allow-bean-definition-overriding=true

workshop.rabbitmq.queue=workshop.events.email
workshop.rabbitmq.exchange=workshop.exchange
workshop.rabbitmq.routingkey=workshop.routingkey
```

Now add the (almost) same configuration as in `service-billing`.
```java
import org.springframework.amqp.rabbit.connection.ConnectionFactory;
import org.springframework.amqp.core.*;
import org.springframework.amqp.rabbit.core.RabbitTemplate;
import org.springframework.amqp.support.converter.Jackson2JsonMessageConverter;
import org.springframework.amqp.support.converter.MessageConverter;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class MessageBrokerConfiguration {

    @Value("${workshop.rabbitmq.queue}")
    private String queueName;
    @Value("${workshop.rabbitmq.exchange}")
    private String exchange;
    @Value("${workshop.rabbitmq.routingkey}")
    private String routingKey;

    @Bean
    public Queue queue() {
        return new Queue(queueName);
    }

    @Bean
    public DirectExchange exchange() {
        return new DirectExchange(exchange);
    }

    @Bean
    Binding binding(Queue queue, DirectExchange exchange) {
        return BindingBuilder.bind(queue).to(exchange).with(routingKey);
    }
}
```

The next part is different from the sender because we are going to create a listener. This will be the entrypoint for what we receive and you can add as many queues as you want, but we will now focus on one.

```java
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.stereotype.Component;

@Component
public class EventReceiver {

    private Logger log = LoggerFactory.getLogger(EventReceiver.class);

    @RabbitListener(queues = "${workshop.rabbitmq.queue}")
    public void receive(String event) {
        System.out.println("received the event!");
        log.info("Received event in service document generation: {}", event);
        // Convert to object.
        // Do with it whatever you please.
    }
}
```

Whenever you added all the above and start all apps (in Docker), you should be able to do a GET request to `service-billing` and a message should be send to and picked up by `service-document-generation` and `service-email`.

## Assignment
Make sure to add all code for sending and listening to the services. Whenever you did this, make sure to build the application and build a new Docker container. Otherwise it will pick up the old one. If you forgot how to build the application, check the lab [Dockerizing our apps](2-dockerizing-our-apps.md) again.

Also write one `docker-compose.yml` file which will start everything at once. If you have everything running, you should see similar output as below.

```
↳ docker ps
CONTAINER ID        IMAGE                                COMMAND                  CREATED              STATUS              PORTS                                                                                        NAMES
a92cc5f27f48        service-email:latest                 "java -cp /app/resou…"   About a minute ago   Up About a minute   0.0.0.0:8083->8080/tcp                                                                       deployment_service-email_1
595399bac8c0        rabbitmq:3.8-management              "docker-entrypoint.s…"   About a minute ago   Up About a minute   4369/tcp, 5671/tcp, 0.0.0.0:5672->5672/tcp, 15671/tcp, 25672/tcp, 0.0.0.0:15672->15672/tcp   deployment_message-broker_1
d85847fd68d5        service-billing:latest               "java -cp /app/resou…"   About a minute ago   Up About a minute   0.0.0.0:8081->8080/tcp                                                                       deployment_service-billing_1
d54a6a434929        service-document-generation:latest   "java -cp /app/resou…"   About a minute ago   Up About a minute   0.0.0.0:8082->8080/tcp                                                                       deployment_service-document-generation_1
```

Do a GET request to `service-billing` and confirm that you received a 200 statuscode.

```
↳ http :8081/start-billing
HTTP/1.1 200 
Connection: keep-alive
Content-Length: 0
Date: Sat, 21 Mar 2020 12:35:25 GMT
Keep-Alive: timeout=60
```

Now check the logging of both `service-email` and `service-document-generation` and verify everything is received. You can view the logs of a Docker container whenever you specify the command `docker logs <container id>`. You can obtain the container id by running `docker ps`.

```
↳ docker logs deployment_service-email_1 -f

...
received the event!
2020-03-21 12:35:25.900  INFO 1 --- [ntContainer#0-3] c.w.serviceemail.receiver.EventReceiver  : Received event in service email: {"customerName":"John Doe","amountInEuros":50}
```

And

```
↳ docker logs deployment_service-document-generation_1 -f

...
received the event!
2020-03-21 12:35:25.901  INFO 1 --- [ntContainer#0-3] c.w.s.receiver.EventReceiver             : Received event in service document generation: {"customerName":"John Doe","amountInEuros":50}
```

Congratulations, you've made it to the end.

## Finishing point
If you want to skip ahead, get the latest code from this assignment.
```
↳ git checkout 5-intercommunication-finished
```

## Next lab
There is no next lab, you're finished!