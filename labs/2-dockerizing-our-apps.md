# Dockerizing our apps

## Previous lab
 * [Setting up our environment](1-setting-up-our-environment.md)

## Starting point
```
↳ git checkout 2-dockerizing-our-apps
```

We're going to Dockerize the apps that we just made in the previous lab. Why? Because then we are able to run it everywhere.

Usually whenever you are creating a Docker container, you need a Dockerfile. That Dockerfile contains the specifics of your container. Right now, we are going to do it the lazy way by adding a Maven dependency to our project that builds a container for us without specifying anything. 

First we are going to add [Jib](https://github.com/GoogleContainerTools/jib/tree/master/jib-maven-plugin) as a dependency to our projects. This will help us make the Docker container without too much hassle.

Start by adding the below snippet to your `pom.xml`-file.

```xml
<project>
...
    <build>
    ...
        <plugins>
        ...
            <plugin>
                <groupId>com.google.cloud.tools</groupId>
                <artifactId>jib-maven-plugin</artifactId>
                <version>2.1.0</version>
                <configuration>
                    <to>
                        <image>service-billing</image>
                    </to>
                </configuration>
            </plugin>
        ...
        </plugins>
    ...
    </build>
...
</project>
```
First run
```
↳ ./mvnw clean install
```

And then run 
```
↳  ./mvnw jib:dockerBuild
```
And you should be able to see similar output as below.

```log
↳ ./mvnw jib:dockerBuild
[INFO] Scanning for projects...
[INFO] 
[INFO] --------------------< com.workshop:service-billing >--------------------
[INFO] Building service-billing 0.0.1-SNAPSHOT
[INFO] --------------------------------[ jar ]---------------------------------
[INFO] 
[INFO] --- jib-maven-plugin:2.1.0:dockerBuild (default-cli) @ service-billing ---
[INFO] 
[INFO] Containerizing application to Docker daemon as service-billing...
[WARNING] Base image 'gcr.io/distroless/java:8' does not use a specific image digest - build may not be reproducible
[INFO] Using base image with digest: sha256:e99eb6cf88ca2df69e99bf853d65f125066730e3e9f7a233bd1b7e3523c144cb
[INFO] 
[INFO] Container entrypoint set to [java, -cp, /app/resources:/app/classes:/app/libs/*, com.workshop.servicebilling.ServiceBillingApplication]
[INFO] 
[INFO] Built image to Docker daemon as service-billing
[INFO] Executing tasks:
[INFO] [==============================] 100,0% complete
[INFO] 
[INFO] ------------------------------------------------------------------------
[INFO] BUILD SUCCESS
[INFO] ------------------------------------------------------------------------
[INFO] Total time:  4.704 s
[INFO] Finished at: 2020-03-19T13:12:26+01:00
[INFO] ------------------------------------------------------------------------
```
This made a ready to use container for us! You can check what images you have in your local repository by typing this command.
```
↳ docker images -a
```

If you pulled containers before or build containers prior to this workshop, it should show up in this list. For your reference, I cleaned my images and this is the output on my machine after building `service-billing`.
```
↳ docker images -a
REPOSITORY          TAG                 IMAGE ID            CREATED             SIZE
service-billing     latest              7669e031cd08        50 years ago        143MB
```
Do the above steps to all of our services so you have below images.

Now we are ready to run them everywhere and scale them as we please!

## Assignment
We want to make a container for all of our services. So the assignment is to have all of the containers built so you will have below output.

```
↳ docker images -a
REPOSITORY                    TAG                 IMAGE ID            CREATED             SIZE
service-billing               latest              7669e031cd08        50 years ago        143MB
service-email                 latest              ad4fc2ffb6ff        50 years ago        143MB
service-document-generation   latest              d122fa32ebac        50 years ago        143MB
```

## Finishing point
If you want to skip ahead, get the latest code from this assignment.
```
↳ git checkout 2-dockerizing-our-apps-finished
```

## Next lab
 * [Running our apps](3-running-our-apps.md)