# Running our apps

## Previous lab
 * [Dockerizing our apps](2-dockerizing-our-apps.md)

## Starting point

```
↳ git checkout 3-running-our-apps
```

Now that we have built our containers, the next step is to run them. For now, we're just going to run `service-billing`.

Run the below command, for which I explained below what it is doing.

```
↳ docker run -p 8080:8080 service-billing:latest
              ^   ^   ^           ^          ^
A.------------|   |   |           |          |
B.----------------|   |           |          |
C.--------------------|           |          |
D.--------------------------------|          |
E.-------------------------------------------|

A = The -p let's you specify to bind to a port.
B = This is the port of the host.
C = This is the port of what is running inside the container.
D = The image you want to run.
E = The tag of the image you want to run.
```

If you run this command, you'll notice that your terminal is showing the application logging, but it also hangs on it. You should see something like below.

```
↳ docker run -p 8080:8080 service-billing:latest

  .   ____          _            __ _ _
 /\\ / ___'_ __ _ _(_)_ __  __ _ \ \ \ \
( ( )\___ | '_ | '_| | '_ \/ _` | \ \ \ \
 \\/  ___)| |_)| | | | | || (_| |  ) ) ) )
  '  |____| .__|_| |_|_| |_\__, | / / / /
 =========|_|==============|___/=/_/_/_/
 :: Spring Boot ::        (v2.2.5.RELEASE)

2020-03-19 14:52:36.873  INFO 1 --- [           main] c.w.s.ServiceBillingApplication          : Starting ServiceBillingApplication on 001cabdba69d with PID 1 (/app/classes started by root in /)
2020-03-19 14:52:36.882  INFO 1 --- [           main] c.w.s.ServiceBillingApplication          : No active profile set, falling back to default profiles: default
2020-03-19 14:52:37.957  INFO 1 --- [           main] o.s.b.w.embedded.tomcat.TomcatWebServer  : Tomcat initialized with port(s): 8080 (http)
2020-03-19 14:52:37.969  INFO 1 --- [           main] o.apache.catalina.core.StandardService   : Starting service [Tomcat]
2020-03-19 14:52:37.970  INFO 1 --- [           main] org.apache.catalina.core.StandardEngine  : Starting Servlet engine: [Apache Tomcat/9.0.31]
2020-03-19 14:52:38.048  INFO 1 --- [           main] o.a.c.c.C.[Tomcat].[localhost].[/]       : Initializing Spring embedded WebApplicationContext
2020-03-19 14:52:38.049  INFO 1 --- [           main] o.s.web.context.ContextLoader            : Root WebApplicationContext: initialization completed in 1081 ms
2020-03-19 14:52:38.234  INFO 1 --- [           main] o.s.s.concurrent.ThreadPoolTaskExecutor  : Initializing ExecutorService 'applicationTaskExecutor'
2020-03-19 14:52:38.416  INFO 1 --- [           main] o.s.b.w.embedded.tomcat.TomcatWebServer  : Tomcat started on port(s): 8080 (http) with context path ''
2020-03-19 14:52:38.419  INFO 1 --- [           main] c.w.s.ServiceBillingApplication          : Started ServiceBillingApplication in 2.096 seconds (JVM running for 2.448)
```

If you exit this terminal you will also notice that the container is exiting. So let's exit this terminal and enter below command.

```
↳ docker ps
CONTAINER ID        IMAGE               COMMAND             CREATED             STATUS              PORTS               NAMES
```

You can see that there is nothing running. You can also run the container in daemon mode. That means that it runs in the background and keeps running until it crashes or you stop it. We can run the container in daemon mode through adding `-d`.

So I'm grabbing the command from earlier and add `-d` to it. You should see something like below. And what you'll also notice is that your terminal does not hang.

```
↳ docker run -d -p 8080:8080 service-billing:latest
1985497ef05ef91176e80cfd54385fcbbdbd4f4272adaae8ed4ff802e2badc6e

↳ docker ps
CONTAINER ID        IMAGE                    COMMAND                  CREATED             STATUS              PORTS                    NAMES
1985497ef05e        service-billing:latest   "java -cp /app/resou…"   4 seconds ago       Up 3 seconds        0.0.0.0:8080->8080/tcp   hardcore_meitner
```

The container will now continue to run in the backhground and is reachable on port 8080.

```
If you want to stop the container, you'll have to run
↳ docker stop <container_id>

So in this case that will be 
↳ docker stop 1985497ef05e
```

## Assignment

Make sure that you run all of the apps on different ports on the host. And that they are reachable. So:

| service                     | port on host | port in container | endpoint |
| --------------------------- | :----------: | ----------------: | -------- |
| service-billing             |     8081     |              8080 | /hello   |
| service-document-generation |     8082     |              8080 | /hello   |
| service-email               |     8083     |              8080 | /hello   |

If you did this right, you should see below output when you do a GET request to the services.

service-billing

```
↳ http :8081/hello
HTTP/1.1 200
Connection: keep-alive
Content-Type: application/json
Date: Thu, 19 Mar 2020 18:10:48 GMT
Keep-Alive: timeout=60
Transfer-Encoding: chunked

{
    "app": "service-billing",
    "message": "Hello"
}
```

service-document-generation

```
↳ http :8082/hello
HTTP/1.1 200
Connection: keep-alive
Content-Type: application/json
Date: Thu, 19 Mar 2020 18:11:02 GMT
Keep-Alive: timeout=60
Transfer-Encoding: chunked

{
    "app": "service-document-generation",
    "message": "Hello"
}
```

service-email

```
↳ http :8083/hello
HTTP/1.1 200
Connection: keep-alive
Content-Type: application/json
Date: Thu, 19 Mar 2020 18:11:15 GMT
Keep-Alive: timeout=60
Transfer-Encoding: chunked

{
    "app": "service-email",
    "message": "Hello"
}
```

## Finishing point

Please note that the code is the same as in the starting point because we poked around with the command-line in this lab.

```
↳ git checkout checkout 3-running-our-apps-finished
```

## Next lab
 * [Connecting it up](4-connecting-it-up.md)
 