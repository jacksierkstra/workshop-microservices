#!/usr/bin/env bash

declare -a branches=(
  '1-setting-up-our-environment'
  '1-setting-up-our-environment-finished'
  '2-dockerizing-our-apps'
  '2-dockerizing-our-apps-finished'
  '3-running-our-apps'
  '3-running-our-apps-finished'
  '4-connecting-it-up'
  '4-connecting-it-up-finished'
  '5-intercommunication'
  '5-intercommunication-finished'
  'master'
)

branchToSyncFrom='final-docs'

for branch in "${branches[@]}"; do
  git checkout $branch
  git checkout $branchToSyncFrom -- labs/
  git add labs/
  git commit -m "Syncing lab files from $branchToSyncFrom to $branch."
  git push origin $branch
done
